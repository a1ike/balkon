"use strict";

jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.b-qa-card__title').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('b-qa-card__title_active');
    $(this).next().slideToggle('fast');
  });
  $('.b-tab_toggle_1').on('click', function (e) {
    e.preventDefault();
    $('#b_tab_2').hide();
    $('#b_tab_1').show();
  });
  $('.b-tab_toggle_2').on('click', function (e) {
    e.preventDefault();
    $('#b_tab_1').hide();
    $('#b_tab_2').show();
  });
});
//# sourceMappingURL=main.js.map